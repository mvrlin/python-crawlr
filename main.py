import json

import asyncio
import requests

from pyppeteer import launch

SITE_DOMAIN = 'https://www.bwcon.de'

fetch_count = 0

async def get_items(url, form, page):
  global fetch_count

  fetch_count = form['tx_bwconlist_bwcon[clickCounter]'] = fetch_count + 1
  response = requests.post(url, data=form)

  if len(response.text) == 0:
    return []

  data = await page.evaluate(
    '''
    () => {
      const parser = new DOMParser()
      const dom = parser.parseFromString(`%s`, 'text/html')

      return [...dom.querySelectorAll('.bwc-panel-content')].reduce((acc, node) => {
        const h3 = node.querySelector('h3 a')

        if (h3) {
          acc.push({
            href: h3.href,
            title: h3.innerText
          })
        }

        return acc
      }, [])
    }
    ''' % response.text
  )

  return data

async def main():
  browser = await launch()
  page = await browser.newPage()

  await page.goto(SITE_DOMAIN + '/aus-dem-netzwerk/meldungen')

  url = await page.evaluate('''
    () => document.getElementById('formLoadMore').getAttribute('action') || ''
  ''')

  if url == '':
    print('No URL found.')
    return browser.close()

  form = await page.evaluate(
    '''
    () => [...document.querySelectorAll('#formLoadMore input')].reduce((acc, n) => {
        acc[n.getAttribute('name')] = n.getAttribute('value')
        return acc
    }, {})
    '''
  )

  items = []
  open('data.json', 'w').close()

  while True:
    data = await get_items(url=SITE_DOMAIN + url, form=form, page=page)

    if len(data) == 0:
      break

    items = items + data

    with open('data.json', 'w') as file:
      json.dump(items, file, indent=2, sort_keys=True)

  await browser.close()

asyncio.get_event_loop().run_until_complete(main())
